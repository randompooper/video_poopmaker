-- Generic script generator
-- Just mashes clips on random. There are some options letting you limit the randomness

local tool = require("tool")
local effect = require("effect")
local helper = require("helper")

-- TO DO: Since helper functions gone we probably have to return generator function
local scripter = {}

-- Script format: { clip = "path/to_clip.mp4", interval = { start, finish }, optional effect = { name = "effect_name", opt = { ... effect options ... } }
-- single_clip will force scripter to generate a script item for that clip specifically (used by effect.overlay)
function scripter.generate(cfg, sources, single_clip)
	local script = {}
	local effect_opts = cfg.effects
	local min_v = cfg.clip_min or 0.5
	local max_v = cfg.clip_max or 1.5
	local duration = 0.0
	local max_duration = single_clip and 9999 or cfg.max_duration or 30.0
	local max_clips = single_clip and 1 or cfg.max_clips or 50
	local prev_interval, prev_made_interval, prev_effect
	local prev_clip = single_clip or cfg.first_clip -- nil value is valid
	local clip_change = single_clip and 0 or cfg.clip_change or 1
	local clip_change_override = cfg.clip_change_override or 1
	local interval_change = cfg.interval_change or 1
	local interval_advance = cfg.interval_advance or 1
	local apply_effect = cfg.apply_effect or 0
	local base_nice = cfg.base_nice or 1
	-- Discard invalid first clip
	if prev_clip ~= nil and tool.duration_cache[prev_clip] == nil and not tool.is_video_with_audio(prev_clip) then
		print('Warning! First clip probing failed', prev_clip)
		prev_clip = nil
	end
	for i = 1,max_clips do
		local scr = {}
		local clip = prev_clip
		local override_clip
		-- Pick new clip with certain probability
		if clip == nil or tool.probability(clip_change) then
			clip = sources[math.random(1, #sources)]
			if prev_clip == nil or tool.probability(clip_change_override) then
				-- Reset last interval
				prev_interval = nil
			else
				-- Current clip won't override prev_clip and prev_interval
				override_clip = true
			end
		end
		local interval
		local clip_len = tool.media_duration(clip)
		-- Either pick random interval or extend/reuse previous
		if override_clip or prev_interval == nil or tool.probability(interval_change) or clip_len - prev_interval[2] < min_v then
			interval = tool.random_interval(clip_len, min_v, max_v)
		else
			-- Use the previously created interval rather than previous interval for clip repetition
			local should_advance = not prev_made_interval or tool.probability(interval_advance)
			interval = should_advance and tool.random_interval(clip_len, min_v, max_v, prev_interval[2]) or prev_made_interval
		end
		-- Apply effect with that probability
		local opt
		if effect_opts ~= nil and tool.probability(apply_effect) then
			repeat
				-- Dumb way to do nice based randomness
				-- Just avoid setting tiny nice values otherwise script generation might hang
				opt = effect_opts[math.random(1, #effect_opts)]
			until tool.probability(opt.nice or base_nice)
		end
		if opt ~= nil then
			-- Special effect option that overrides clip entirely
			if opt.name == 'clip_override' then
				-- Don't conflict with current override
				if not override_clip then
					override_clip = helper.extract_value(opt.opt, clip, sources, interval, cfg)
					if type(override_clip) == 'table' then
						override_clip = override_clip[math.random(1, #override_clip)]
					end
					local override_duration = tool.media_duration(override_clip)
					-- If fails then the clip that we have picked above will be used without effect
					if override_duration ~= nil then
						-- Use clip entirely
						interval = nil
						clip = override_clip
					else
						override_clip = nil
					end
				end
			else
				if effect[opt.name] == nil then
					error("Invalid effect name: "..tostring(opt.name))
				end
				-- Process and fill effect options
				local scr_effect_opt = {}
				local scr_opt = { name = opt.name, opt = scr_effect_opt }
				-- TO DO: UGLY NAMING
				local scr_opt_opt = helper.extract_value(opt.opt, clip, sources, interval, cfg)
				if scr_opt_opt ~= nil then
					for name,v in pairs(scr_opt_opt) do
						scr_effect_opt[name] = helper.extract_value(v, clip, sources, interval, cfg)
					end
				end
				scr.effect = scr_opt
			end
		end
		-- Store real clip interval because prev_interval might be extended below
		-- It's required for the clip repetition to work properly
		prev_made_interval = interval
		-- Extend previous clip interval if new script entry logically extends it
		if clip == prev_clip and interval and prev_interval and
			prev_interval[2] == interval[1] and opt == nil and prev_effect == opt
		then
			-- 100% dark magic free: prev_interval is tied to previous clip
			prev_interval[2] = interval[2]
		else
			if not override_clip then
				prev_effect = opt
				prev_interval = interval
				prev_clip = clip
			end
			scr.interval = interval
			scr.clip = clip
			table.insert(script, scr)
		end
		-- Don't exceed our configured time!
		-- The hook below might change interval and scr.effect
		duration = duration + helper.hook_clip_duration(clip, interval, scr.effect)
		if duration >= max_duration then
			break
		end
	end
	return script
end

return scripter
