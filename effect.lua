-- Effect processing only
-- Don't randomize effect behavior here! Do it at script or config level whenever possible

local tool = require("tool")

local effect = {}
-- Each effect function takes 3 arguments: input_path, output_path, effect_opts
-- Optional helper function effect.[effect_name]_hook have final say on clip and effect parameters. Must return approximate duration of the produced clip

function effect.reverse(clip, clip_out)
	return tool.ffmpeg('-i %s -af areverse -vf reverse -y %s',
		tool.escape_path(clip), tool.escape_path(clip_out))
end

-- float speed - from 0.5 to 2.0
-- bool with_pitch - Speeding up will affect pitch when true
function effect.speed(clip, clip_out, e)
	local cmd = e.with_pitch and '-i %s -vf setpts=%f*PTS -af asetrate=44100*%f,aresample=44100 -y %s' or '-i %s -vf setpts=%f*PTS -af atempo=%f -y %s'
	return tool.ffmpeg(cmd, tool.escape_path(clip), 1 / e.speed, e.speed, tool.escape_path(clip_out))
end

function effect.speed_hook(clip, duration, interval, e)
	if not interval then
		-- We can't manipulate interval. Return adjusted duration
		return duration / e.speed
	end
	-- Adapt to clip interval
	interval[2] = interval[1] + duration * e.speed
	return duration
end

-- float scale - pitch scale from 0.5 to 2.0
function effect.pitch(clip, clip_out, e)
	return tool.ffmpeg('-i %s -c:v copy -af asetrate=44100*%f,atempo=%f,aresample=44100 -y %s',
		tool.escape_path(clip), e.scale, 1.0 / e.scale, tool.escape_path(clip_out))
	-- The solution below is better and produces higher quality result but requires ffmpeg with rubberband support
	--return tool.ffmpeg('-i %s -c:v copy -af rubberband=pitch-scale=%f -y %s',
	--	tool.escape_path(clip), e.scale, tool.escape_path(clip_out))
end

function effect.chorus(clip, clip_out)
	-- TO DO: Let user customize/randomize
	return tool.ffmpeg('-i %s -c:v copy -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -y %s',
		tool.escape_path(clip), tool.escape_path(clip_out))
end

-- float f - Recommended to be 7.0
-- float d - Recommended to be 0.5
function effect.vibrato(clip, clip_out, e)
	-- TO DO: Sometimes fails with short clips. Not using aac seem to fix that issue
	return tool.ffmpeg('-i %s -c:v copy -af vibrato=f=%f:d=%f -y %s',
		tool.escape_path(clip), e.f, e.d, tool.escape_path(clip_out))
end

-- string path - Path to sound
-- float len - Sound duration
function effect.sound(clip, clip_out, e)
	local sound_path = e.sound[1]
	local interval = e.sound[2]
	if type(interval) == 'table' then
		-- We must cut the sound ourselves!
		local tmp_name = tool.temp_name('.m4a')
		local success = tool.ffmpeg('-ss %f -to %f -i %s -vn -ac 1 -af aresample=44100 -y %s',
			interval[1], interval[2], tool.escape_path(sound_path), tool.escape_path(tmp_name))
		return success and tool.ffmpeg('-i %s -i %s -fflags shortest -map 0:v -map 1:a -y %s',
			tool.escape_path(clip), tool.escape_path(tmp_name), tool.escape_path(clip_out))
	end
	return tool.ffmpeg('-i %s -i %s -fflags shortest -map 0:v -map 1:a -ac 1 -af aresample=44100 -shortest -y %s',
		tool.escape_path(clip), tool.escape_path(sound_path), tool.escape_path(clip_out))
end

function effect.sound_hook(clip, duration, interval, e)
	local sound_interval = e.sound[2]
	local sound_duration
	if type(sound_interval) == 'table' then
		sound_duration = sound_interval[2] - sound_interval[1]
	else
		sound_duration = tool.media_duration(e.sound[1])
	end
	return math.min(sound_duration, duration)
end

-- bool flip - Uses different side of the video for mirroring
function effect.mirror(clip, clip_out, e)
	-- TO DO: Vertical mirror too
	local flip = e.flip
	return tool.ffmpeg('-i %s -c:a copy -vf "crop=iw/2:ih:%s,split[part0][tmp];[tmp]hflip[part1];%shstack" -y %s',
		tool.escape_path(clip), flip and '0:0' or 'iw/2:ih', flip and '[part0][part1]' or '[part1][part0]', tool.escape_path(clip_out))
end

function effect.gmajor(clip, clip_out)
	return tool.ffmpeg('-i %s -vf negate -filter_complex "[0]asetrate=44100*2.0, aresample=44100, atempo=1/2[s0];[0]asetrate=44100*1.498307, aresample=44100, atempo=1/1.498307[s1];[0]asetrate=44100*1.259921, aresample=44100, atempo=1/1.259921[s2];[0]asetrate=44100*0.749154, aresample=44100, atempo=1/0.749154[s3];[0]asetrate=44100*0.5, aresample=44100, atempo=1/0.5[s4];[s0][s1][s2][s3][s4][0]amix=6[pf];[pf]volume=5.0:precision=fixed[f]" -map 0:v -map [f] -y %s',
		tool.escape_path(clip), tool.escape_path(clip_out))
end

-- float scale - Scale amount
-- int x - x offset
-- int y - y offset
function effect.scale(clip, clip_out, e)
	local v = tool.video_size
	return tool.ffmpeg('-i %s -c:a copy -vf "scale=%f*iw:-1,crop=%d:%d:%d:%d,setsar=1:1,fps=fps=30" -y %s',
		tool.escape_path(clip), e.scale, v.w, v.h, e.x, e.y, tool.escape_path(clip_out))
end

-- table scr - Clip script element produced by script.lua
-- int size - Overlay size
-- int x - Overlay position x
-- int y - Overlay position y
-- bool force_stretch - Force tool.scale_stretch for overlay clips
function effect.overlay_clip(clip, clip_out, e)
	local clip_tmp = tool.temp_name()
	local process_clip = require("clip_processor")
	local scale_stretch = tool.scale_stretch
	if e.force_stretch then
		tool.scale_stretch = true
	end
	local success = process_clip(e.scr, clip_tmp)
	if e.force_stretch then
		tool.scale_stretch = scale_stretch
	end
	success = tool.ffmpeg('-i %s -i %s -filter_complex "[1]scale=%d:-1[s1];[0][s1]overlay=%d:%d:enable=1:shortest=1[fv];[0][1]amix=2:duration=shortest,aresample=44100[fa]" -map [fa] -map [fv] -ac 1 -y %s',
		tool.escape_path(clip), tool.escape_path(clip_tmp), e.size, e.x, e.y, tool.escape_path(clip_out))
	if success then
		-- Very short clips messes with the output video and concat fails (though the result is still playable)
		-- We have to reconvert it to make it usable for concat
		os.rename(clip_out, clip)
		return tool.copy_clip(clip, clip_out)
	end
	return success
end

-- int count - Amount of mini clips
-- string music - Path to dance music
-- table interval - Music interval
function effect.dance(clip, clip_out, e)
	local forward_cut = tool.temp_name()
	local backward_cut = tool.temp_name()
	local sound_cut = tool.temp_name('.m4a')
	local success = tool.ffmpeg('-ss %f -to %f -i %s -vn -ac 1 -af aresample=44100 -y %s',
		e.interval[1], e.interval[2], tool.escape_path(e.music), tool.escape_path(sound_cut))
	if not success then return false end
	success = tool.ffmpeg('-i %s -map 0 -an -to %f -y %s',
		tool.escape_path(clip), (e.interval[2] - e.interval[1]) / e.count, tool.escape_path(forward_cut))
	if not success then return false end
	success = tool.ffmpeg('-i %s -map 0 -vf reverse -y %s',
		tool.escape_path(forward_cut), tool.escape_path(backward_cut))
	if not success then return false end
	local clip_format = ''
	local c = 0
	for i = 1,e.count do
		clip_format = clip_format..string.format('[%d:v:0]', c)
		-- I know it is simplier to write (i % 2) or math.mod(i, 2) but that is least ass way to be compatible with Lua 5.1
		c = c == 0 and 1 or 0
	end
	return tool.ffmpeg('-i %s -i %s -i %s -fflags shortest -filter_complex "%sconcat=n=%d:v=1,setpts=0.5*PTS[outv]" -map [outv] -map 2 -to %f -shortest -y %s',
		tool.escape_path(forward_cut), tool.escape_path(backward_cut), tool.escape_path(sound_cut), clip_format, e.count, e.interval[2] - e.interval[1], tool.escape_path(clip_out))
end

function effect.dance_hook(clip, duration, interval, e)
	return math.min(duration, e.interval[2] - e.interval[1])
end

return effect
