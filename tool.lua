-- Various ease of development functionality

local lfs = require("lfs")

local tool = {}

local OS = (function ()
	local bin = package.cpath:match("%p[\\|/]?%p(%a+)")
	if bin == "dll" then
		return "Win"
	elseif bin == "so" then
		return "Linux"
	elseif bin == "dylib" then
		return "OSX"
	end
end)()

setmetatable(tool, {
	__index = function(t, v)
		local e = t.exec[v]
		if e ~= nil then
			local f = function(cmd, ...)
				--io.stderr:write(e, ' ', cmd, ' ', ...)
				--io.stderr:write('\n')
				-- TO DO: Either custom string.format or modify it by detecting '%P' format - path string to escape with tool.escape_path
				cmd = string.format('%s '..cmd..' 2>&1', e, ...)
				local f = io.popen(cmd)
				local out = f:read('a')
				local success = f:close()
				if not success then
					io.stderr:write(cmd..'\n'..out..'\n')
				--else
				--	io.stderr:write(cmd..'\n')
				end
				return success
			end
			t[v] = f
			return f
		end
	end
})

-- Executable utilities go here. Later you call tool with utility name (like tool.ffmpeg(args...))
tool.exec = {
	lua = "lua", -- Path to Lua interpreter (TO DO: Find out using Lua itself ?)
	ffmpeg = "ffmpeg",
	ffprobe = "ffprobe"
}
-- All useful paths go there
tool.paths = {
	tmp = "./tmp"
}
-- Media extension. Used for clips and for output result
-- Only mp4 is supported at the moment. Other formats won't concat likely
tool.ext = ".mp4"
-- Media resolution
tool.video_size = { w = 640, h = 480 }
-- Stretch video with aspect ratio differing from our video size
-- TO DO: Automatic stretch mode: stretch only if aspect ratio difference is low
tool.scale_stretch = false
-- TO DO: codec configuration

-- Formats string and prints
function tool.printf(...)
	print(string.format(...))
end

-- Returns temporary name relative to configured temp directory
function tool.temp_name(ext)
	if ext == nil then
		ext = tool.ext
	end
	local name
	-- Loop to avoid extremely rare collision
	-- If those happen very likely your random is broken or your script is messing up with its seed
	repeat
		name = string.format("%s/%u%s", tool.paths.tmp, math.random(0, math.maxrandint), ext)
	until lfs.attributes(name, 'mode') == nil
	return name
end

-- Escapes path making it suitable for use as argument in os.execute or io.popen
-- TO DO: On Windows convert path from UTF-8 to ANSI or warn when UTF-8 path contains unicode chars
--        At the moment you can work it around by converting all luas to ANSI
function tool.escape_path(path)
	return '"'..path:gsub('"', '\\"')..'"'
end

tool.duration_cache = {}
-- Gets media file duration in seconds
function tool.media_duration(path, ignore_cache)
	local v
	if not ignore_cache then
		v = tool.duration_cache[path]
	end
	if v == nil then
		local f = io.popen(tool.exec.ffprobe.." -i "..tool.escape_path(path).." -show_entries format=duration -v error -of csv=p=0")
		v = f:read("n")
		f:close()
		-- Invalid duration. Force fail
		if v and v <= 0 then
			v = nil
		end
		tool.duration_cache[path] = v
	end
	return v
end
-- Verifies that media contains both video and audio tracks
-- On success returns media duration
function tool.is_video_with_audio(path)
	local f = io.popen(tool.exec.ffprobe..' -i '..tool.escape_path(path)..' -loglevel error -show_entries stream=codec_type -of csv=p=0')
	local video, audio
	for line in f:lines() do
		if line == 'video' then
			video = true
		elseif line == 'audio' then
			audio = true
		else
			break
		end
	end
	f:close()
	return video and audio and tool.media_duration(path, true)
end

-- Crops the clip and saves it to clip_out
-- With nil interval it copies whole clip
function tool.crop_clip(clip, interval, clip_out)
	local v = tool.video_size
	local success
	if interval ~= nil then
		if tool.scale_stretch then
			success = tool.ffmpeg('-ss %f -to %f -i %s -fflags shortest -vf "scale=%dx%d,setsar=1:1,fps=fps=30" -ac 1 -af aresample=44100 -y %s',
				interval[1], interval[2], tool.escape_path(clip), v.w, v.h, tool.escape_path(clip_out))
		else
			success = tool.ffmpeg('-ss %f -to %f -i %s -fflags shortest -vf "scale=%d:%d:force_original_aspect_ratio=decrease,pad=%d:%d:(ow-iw)/2:(oh-ih)/2,setsar=1:1,fps=fps=30" -ac 1 -af aresample=44100 -y %s',
				interval[1], interval[2], tool.escape_path(clip), v.w, v.h, v.w, v.h, tool.escape_path(clip_out))
		end
	else
		if tool.scale_stretch then
			success = tool.ffmpeg('-i %s -vf "scale=%dx%d,setsar=1:1,fps=fps=30" -ac 1 -af aresample=44100 -y %s',
				tool.escape_path(clip), v.w, v.h, tool.escape_path(clip_out))
		else
			success = tool.ffmpeg('-i %s -vf "scale=%d:%d:force_original_aspect_ratio=decrease,pad=%d:%d:(ow-iw)/2:(oh-ih)/2,setsar=1:1,fps=fps=30" -ac 1 -af aresample=44100 -y %s',
				tool.escape_path(clip), v.w, v.h, v.w, v.h, tool.escape_path(clip_out))
		end
	end
	-- We have to bust clip_out from the cache in all cases otherwise something very unexpected can happen
	tool.duration_cache[clip_out] = nil
	if success then
		return tool.media_duration(clip_out)
	end
end

-- Copies clip fully with specific to our program formatting
function tool.copy_clip(clip, clip_out)
	return tool.crop_clip(clip, nil, clip_out)
end

-- Tests your luck by a chance
function tool.probability(chance)
	if chance >= 1.0 then
		return true
	elseif chance <= 0.0 then
		return false
	end
	return chance >= math.random()
end

-- Generates random interval
function tool.random_interval(len, min_v, max_v, start)
	if min_v > len then
		return { 0.00, len }
	end
	if max_v > len then
		max_v = len
	end
	if start == nil then
		start = math.random() * (len - min_v)
	end
	local finish = start + min_v + math.random() * math.min(max_v - min_v, len - start - min_v)
	return { start, finish }
end

-- Lists files in current directory
function tool.list_files(dir)
	local files = {}
	for file in lfs.dir(dir) do
		if file ~= '.' and file ~= '..' then
			local full_name = dir..'/'..file
			if lfs.attributes(full_name, 'mode') == 'file' then
				table.insert(files, full_name)
			end
		end
	end
	return files
end

-- List files in current and child directories
function tool.list_files_recurse(dir, files)
	if files == nil then
		files = {}
	end
	for file in lfs.dir(dir) do
		if file ~= '.' and file ~= '..' then
			local full_name = dir..'/'..file
			local attr = lfs.attributes(full_name, 'mode')
			if attr == 'file' then
				table.insert(files, full_name)
			elseif attr == 'directory' then
				tool.list_files_recurse(full_name, files)
			end
		end
	end
	return files
end

-- Splits table to count tables
function tool.split_table(tt, count)
	if #tt <= count then
		return { tt }
	end
	local ret = {}
	local cur = 1
	local left = #tt
	for i=1,count do
		local t = {}
		local ps = math.ceil(#tt / count)
		table.move(tt, cur, cur + ps - 1, 1, t)
		cur = cur + ps
		table.insert(ret, t)
	end
	return ret
end

-- Serializes table into a loadable Lua string
-- Some code copied from: lua-users.org/wiki/TableSerialization
function tool.serialize_table(tt, done)
	-- Avoid infinite recursion by not parsing already parsed table values
	done = done or {}
	local sb = {'{'}
	for key, value in pairs(tt) do
		local vt = type(value)
		local kt = type(key)
		if vt == "table" then
			if not done[value] then
				done[value] = true
				if kt ~= 'number' then
					table.insert(sb, key .. "=")
				end
				table.insert(sb, tool.serialize_table(value, done))
				table.insert(sb, ",")
				done[value] = nil
			end
		-- It doesn't care about real order though
		elseif kt == 'number' then
			if vt == 'string' then
				value = tool.escape_path(value)
			else
				value = tostring(value)
			end
			table.insert(sb, value..',')
		else
			if vt == 'string' then
				value = tool.escape_path(value)
			else
				value = tostring(value)
			end
			if kt == 'string' then
				key = tool.escape_path(key)
			end
			table.insert(sb, string.format("[%s]=%s,", tostring(key), value))
		end
	end
	table.insert(sb, '}')
	return table.concat(sb)
end

function tool.load_lua_table(path)
	local f = io.open(path, 'r')
	if f == nil then
		return
	end
	local ret = load('return '..f:read('a'), path, 't', {})()
	f:close()
	return ret
end

function tool.write_lua_table(path, tt)
	local f = io.open(path, 'w')
	if f == nil then
		return
	end
	f:write(tool.serialize_table(tt))
	f:close()
	return true
end

return tool
