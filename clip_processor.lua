-- Clip processor function shared by effect.lua and worker.lua
local tool = require("tool")
local effect = require("effect")

return function(scr, clip)
	local clip_tmp = tool.temp_name()
	local interval = scr.interval
	-- Cut the clip
	local success = tool.crop_clip(scr.clip, interval, clip_tmp)
	if success then
		local eff = scr.effect
		local effect_success = eff == nil or effect[eff.name](clip_tmp, clip, eff.opt)
		if not effect_success then
			io.stderr:write("Effect "..eff.name.." failed on clip "..clip.."\n");
		end
		if eff ~= nil and effect_success then
			-- Effect processors shall fix "broken" clips themselves when really necessary
			--tool.copy_clip(clip, clip_tmp)
			--os.rename(clip_tmp, clip)
			--os.remove(clip_tmp)
		else
			os.rename(clip_tmp, clip)
		end
	else
		io.stderr:write("Failed to crop clip "..clip.."\n");
	end
	return success
end
