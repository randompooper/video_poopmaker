-- Parallel worker that can batch verify clips or script elements
-- TO DO: Parallelism with lanes ?!

-- Minor adjustments to environment
require("core")
-- Tools
local tool = require("tool")
-- Find out what we have to do
local cmd = loadfile(arg[1], 't', {})()

if cmd.op == 'verify' then
-- Probe media and return successfully probed ones
local ret = {}
tool.duration_cache = cmd.duration_cache
-- Do media probing and return the result
for _,path in pairs(cmd.list) do
	local v = tool.duration_cache[path]
	-- TO DO: Store audio/video presence as attribute in cache ?
	--        tool.media_duration works for both audio only and video only media and results will be stored in duration cache
	--        With enabled cache_probe it's possible to include already cached audio track by mistake and related errors very likely will be visible at concatenation level only
	if not v then
		v = tool.is_video_with_audio(path) or nil
		if not v then io.stderr:write('Validation failed: '..path..'\n') end
	end
	ret[path] = v
end
io.write(tool.serialize_table(ret))

elseif cmd.op == 'process' then
-- Process clips

-- Restore tool parameters from host process
for k,v in pairs(cmd.env) do
	tool[k] = v
end

local process = require("clip_processor")
local clips = {}
local seg = cmd.segment
local tmp = tool.paths.tmp
local ext = tool.ext
-- Make shit
for i,scr in pairs(cmd.script) do
	local clip = tmp..'/'..tostring(i + seg)..ext
	if process(scr, clip) then
		table.insert(clips, clip)
	end
end
io.write(tool.serialize_table({ clips = clips, duration_cache = cmd.cache_probe and tool.duration_cache or nil }))

else -- process
-- FREEMAN YOU FOOL
error("Unknown operation: "..tostring(command.op))
end
