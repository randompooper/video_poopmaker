#!/bin/sh
# Redirect stdin from /dev/null to avoid workers from screwing up terminal
exec lua make.lua </dev/null
