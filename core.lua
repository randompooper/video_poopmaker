-- Warn when global is accessed without global definition: it's very likely a mistake
-- Warn when global we don't know about is defined: likely a mistake too
-- Require compat53 if Lua 5.1 is running (Windows and/or LuaJIT users)

-- With Lua 5.4 it can be huge
math.maxrandint = math.maxinteger
if _VERSION == 'Lua 5.1' or _VERSION == 'Lua 5.2' then
	require('compat53')
end
if _VERSION == 'Lua 5.1' or _VERSION == 'Lua 5.2' or _VERSION == 'Lua 5.3' then
	-- Good random is needed to prevent tmp.temp_name collisions (os.time() won't do)
	-- Random name generated by os.tmpname() looks good enough
	local name = os.tmpname()
	-- Remove immediately: we only need the name
	os.remove(name)
	-- We are interested only in last 6 characters
	local seed = 0
	for k,v in pairs({string.byte(name, #name - 6, #name)}) do
		seed = seed + v * math.pow(15, k - 1)
	end
	-- This random generator sucks anyway but at least clip generation works
	math.randomseed(seed)
	-- You can't call math.random with huge intervals in Lua 5.1 (but you still can in LuaJIT)
	math.maxrandint = math.pow(2, 31) - 2
	-- Lua 5.4 compat
	function warn(msg, ...)
		-- Always on, ignore
		if msg:find('^@') then
			return
		end
		io.stderr:write('Lua warning: ')
		io.stderr:write(msg, ...)
		io.stderr:write('\n')
	end
end
-- Turn on warnings. We will use those
warn("@on")
-- TRIGGER THE CONDUCTOR
function CONDUCTOR(...)
	if not ... then
		print(...)
		error("CONDUCTOR WE HAVE A PROBLEM!")
	end
	return ...
end
-- Warn every time a global variable is accessed without explicit declaration
-- Note: some_global_var = nil won't exist in _G but will be checked by explicitly_defined and won't warn
local explicitly_defined = {}
local quiet_defs = { lfs = true }
setmetatable(_G, {
	__index = function(t, k)
		if not explicitly_defined[k] then
			warn(debug.traceback('Global variable is accessed without explicit declaration: "'..tostring(k)..'"'))
		end
	end,
	__newindex = function(t, k, v)
		if not quiet_defs[k] then
			warn('Globally defining ', tostring(k), ' = ', tostring(v))
		end
		explicitly_defined[k] = true
		rawset(t, k, v)
	end
})
