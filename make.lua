-- Main script that must be executed with lua make.lua
-- Loads config, generates script and then processes it

-- Performs minor adjustments to environment
require("core")
-- Primary configuration method is through config.lua
-- Here you set paths to utils, configure effects and other options
local cfg = require("config")
-- Various tools and ease of programming
local tool = require("tool")
-- Effect tools
local effect = require("effect")
-- LuaFileSystem
local lfs = require("lfs")

-- Read source list from the config
local sources = cfg.sources
if type(sources) ~= 'table' then
	local path = sources
	if type(path) ~= 'string' then
		error("You forgot to configure sources at config.lua (field sources isn't of type string or table)")
	end
	sources = {}
	local attr = lfs.attributes(path, 'mode')
	if attr == 'file' then
		for source in io.lines(path) do
			table.insert(sources, source)
		end
	elseif attr == 'directory' then
		tool.list_files_recurse(path, sources)
	end
end
-- TO DO: Specify operation on command line maybe ?
local function create_worker(send)
	local cmd_path = os.tmpname()
	-- io.popen can only work one way: either write or read only
	-- Send our task in temporary file and read response instead
	local cmd_f = io.open(cmd_path, 'w')
	cmd_f:write('return '..tool.serialize_table(send)..'\n')
	cmd_f:close()
	return { proc = io.popen(string.format('%s worker.lua %s', tool.exec.lua, cmd_path), 'r'), path = cmd_path }
end

local function process_worker(worker)
	local ret = load('return '..worker.proc:read('a'))()
	worker.proc:close()
	os.remove(worker.path)
	return ret
end

if cfg.cache_probe then
	local duration_cache = loadfile('probed_media', 't', {})
	if duration_cache then
		tool.duration_cache = duration_cache()
	end
end
-- Validate sources before using them
do
	-- TO DO: Maybe use lanes for paralleling purposes. Though it'll require to adapt code specifically for lanes
	local work = tool.split_table(sources, cfg.parallel_probe)
	local workers = {}
	for i,w in pairs(work) do
		if #w > 0 then
			table.insert(workers, create_worker({ op = 'verify', list = w, duration_cache = tool.duration_cache }))
		end
	end
	-- Replace old sources with ones passing the check
	sources = {}
	for i,worker in pairs(workers) do
		local ret = process_worker(worker)
		for path,v in pairs(ret) do
			tool.duration_cache[path] = v
			table.insert(sources, path)
		end
	end
end
if #sources == 0 then
	error("No sources!")
end
local function save_duration_cache()
	if cfg.cache_probe then
		local f = io.open('probed_media', 'w')
		-- TO DO: We probably don't want to write entries related to our temporary directory (should we mark those ?)
		f:write('return '..tool.serialize_table(tool.duration_cache))
		f:close()
	end
end
-- Save just probed media
save_duration_cache()
-- Generate script
local script = {}
local out_name = cfg.out_name
-- Verify if out_name collides
if lfs.attributes(out_name..tool.ext, 'mode') or lfs.attributes(out_name..'.script', 'mode') then
	error('Files might be overwritten with that out name: '..out_name)
end
if cfg.script_path == nil or lfs.attributes(cfg.script_path, 'mode') ~= 'file' then
	local script_cfg = cfg.script
	-- Prioritize duration parameters from main config
	if cfg.max_clips then script_cfg.max_clips = cfg.max_clips end
	if cfg.max_duration then script_cfg.max_duration = cfg.max_duration end
	local scripter = require(cfg.script_generator)
	-- Ensure the scripter will be used by a whole program
	require("helper").scripter = scripter
	script = scripter.generate(script_cfg, sources)
	-- Dump script, chief
	if cfg.dump_script then
		local scr_name = out_name..'.script'
		tool.write_lua_table(scr_name, script)
		print('Script dumped to '..scr_name)
	end
else
	print('Loading script '..cfg.script_path)
	script = tool.load_lua_table(cfg.script_path)
end
tool.printf('Script processing begins (%d entries)', #script)
-- Split script and send its parts to workers for parallel processing
local clips = {}
do
	local work = tool.split_table(script, cfg.parallel)
	local workers = {}
	local env = {
		duration_cache = tool.duration_cache,
		paths = tool.paths,
		exec = tool.exec,
		ext = tool.ext,
		scale_stretch = tool.scale_stretch,
		video_size = tool.video_size
	}
	local seg = #work[1]
	for i,w in pairs(work) do
		if #w > 0 then
			table.insert(workers, create_worker({
				op = 'process',
				env = env, script = w,
				segment = (i - 1) * seg,
				cache_probe = cfg.cache_probe }))
		end
	end
	for i,w in pairs(workers) do
		local ret = process_worker(w)
		for _,clip in pairs(ret.clips) do
			table.insert(clips, clip)
		end
		if ret.duration_cache ~= nil then
			for k,v in pairs(ret.duration_cache) do
				tool.duration_cache[k] = v
			end
		end
	end
end
tool.printf('Script processed, merging (%d clips)', #clips)
local out_path = out_name..tool.ext
if cfg.fast_concat then
	local concat_path = tool.paths.tmp..'/concat.txt'
	local f = io.open(concat_path, 'w')
	-- Concatenate the result
	for i = 1,#clips do
		local clip = clips[i]
		if tool.is_video_with_audio(clip) then
			f:write(string.format("file '%s'\n", clip:gsub(tool.paths.tmp..'/', '')))
		else
			print('Probing failed: '..clip)
		end
	end
	f:close()
	tool.ffmpeg('-f concat -i %s %s', tool.escape_path(concat_path), tool.escape_path(out_path))
else
	-- TO DO: We can split the work like we do above for cases when a large clip set is about to be processed. Command line length is limited too
	local count = 0
	local cmd_clips = ''
	for i = 1,#clips do
		local clip = clips[i]
		if tool.is_video_with_audio(clip) then
			cmd_clips = cmd_clips..' -i '..tool.escape_path(clip)
			count = count + 1
		else
			print('Probing failed: '..clip)
		end
	end
	local cmd_filter = ''
	for i = 0,count - 1 do
		cmd_filter = cmd_filter..string.format('[%d:v:0][%d:a:0]', i, i)
	end
	tool.ffmpeg('%s -filter_complex %sconcat=n=%d:v=1:a=1[outv][outa] -map [outv] -map [outa] -y %s', cmd_clips, cmd_filter, count, tool.escape_path(out_path))
end
-- Save probed media and some other probed content
save_duration_cache()
if not cfg.keep_tmp then
	print('Cleaning up')
	local tmp = tool.paths.tmp
	local count = 0
	for file in lfs.dir(tmp) do
		if file ~= '.' and file ~= '..' then
			local full_file = tmp..'/'..file
			if lfs.attributes(full_file, 'mode') == 'file' then
				os.remove(full_file)
				count = count + 1
			end
		end
	end
	tool.printf('Removed %d trash files', count)
end
-- Enjoy your shit
print(out_path)
if cfg.end_exec then
	os.execute(string.format(cfg.end_exec, out_path))
end
