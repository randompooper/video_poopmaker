-- Universal generator config file

require("core")
local lfs = require("lfs")
local tool = require("tool")
local helper = require("helper")
-- UGLY
--tool.scale_stretch = true

local function probability_clamp(v)
	return math.max(math.min(v, 1), 0)
end

local gnome = 'gnome.mp4'
function helper.gnome_overlay_clip(clip, clips, interval, cfg)
	local scr = helper.scripter.generate(cfg, clips, gnome)
	if scr ~= nil then
		-- No matter what try to use original clip at the full length
		scr.clip = gnome
		-- nil interval instructs clip processor to use clip entirely
		scr.interval = nil --{ 0.00, tool.media_duration(gnome) }
	end
	local v = tool.video_size
	return {
		scr = scr,
		x = math.random(0, v.w - 200), y = math.random(0, v.h - 150),
		size = math.random(120, 240),
		force_stretch = true
	}
end

local function random_script_config(clips)
	local cfg = {}
	cfg.clip_min = helper.random_float(0.3, 1.2)
	cfg.clip_max = helper.random_float(cfg.clip_min, 3)
	cfg.clip_change = probability_clamp(helper.random_float(-1, 2))
	cfg.clip_change_override = probability_clamp(helper.random_float(-1, 2))
	cfg.interval_change = helper.random_float(0, 1)
	cfg.interval_advance = helper.random_float(0.2, 1)
	cfg.apply_effect = helper.random_float(0, 0.9)
	cfg.effects = {}
	--cfg.base_nice = random_float(0.5, 1)
	local all_effects = {
		-- Reverses the video
		{ name = 'reverse' },
		-- Makes video more "scary"
		{ name = 'gmajor' },
		-- Applies mirror effect
		{ name = 'mirror', opt = { flip = 'helper.random_bool()' } },
		-- Scales random video part
		{ name = 'scale', opt = 'helper.random_scale()' },
		-- Random speed
		{ name = 'speed', opt = { with_pitch = 'helper.random_bool()', speed = 'helper.random_float(0.5, 2)' } },
		-- Random audio pitch without speed change
		{ name = 'pitch', opt = { scale = 'helper.random_float(0.5, 2)' } },
		-- Persisted random speed
		{ name = 'speed', opt = { with_pitch = 'helper.random_bool()', speed = function() return helper.random_float(0.5, 2) end } },
		-- Persisted random pitch
		{ name = 'pitch', opt = { scale = function() return helper.random_float(0.5, 2) end } },
		-- Applies chorus effect to audio (configurable options soon)
		{ name = 'chorus' },
		-- Applies vibrato effect to audio (with randomizable options)
		{ name = 'vibrato', opt = { f = 'helper.random_float(7.0, 14.0)', d = 'helper.random_float(0.5, 1.0)' } },
		-- Applies random sound from clip
		{ name = 'sound', opt = { sound = 'helper.random_clip_sound(...)' } },
		-- WOOOOH
		--{ name = 'clip_override', opt = gnome },
		-- WOOOH placed anywhere on the video
		--{ name = 'overlay_clip', opt = 'helper.gnome_overlay_clip(...)' },
		-- Applies random sound
		--{ name = 'sound', opt = { sound = 'helper.random_sound("sounds")' } },
		-- Random clip from anywhere
		-- TO DO
		-- Overlays clip
		{ name = 'overlay_clip', opt = 'helper.random_overlay_clip(...)' },
		-- DANCE
		--{ name = 'dance', nice = 0.5, opt = 'helper.random_dance("music", 3, 8, ...)' },
	}
	local function writable_type(v)
		return type(v) == 'nil' or type(v) == 'string' or type(v) == 'number' or type(v) == 'table' or type(v) == 'boolean'
	end
	for i = 1,math.random(1,20) do
		local eff = all_effects[math.random(1, #all_effects)]
		local opt = eff.opt
		if type(opt) == 'function' then
			opt = opt()
		end
		if type(opt) == 'table' then
			local new_opt = {}
			for k,v in pairs(opt) do
				if type(v) == 'function' then
					v = v()
				end
				CONDUCTOR(writable_type(v), type(v))
				new_opt[k] = v
			end
			opt = new_opt
		end
		CONDUCTOR(writable_type(opt), type(opt))
		eff.opt = opt
		table.insert(cfg.effects, eff)
	end
	local rnd_name
	repeat
		rnd_name = string.format('configs/%u.script_cfg', math.random(0, math.maxrandint))
	until lfs.attributes(rnd_name, 'mode') == nil
	print('Saving random script config to: '..rnd_name)
	tool.write_lua_table(rnd_name, cfg)
	return cfg
end

local function load_script_config(path)
	local cfg = tool.load_lua_table(path)
	print('Loaded script config '..path, cfg)
	return cfg
end

local function fixate_clip(cfg, clip)
	cfg.first_clip = clip
	cfg.clip_change_override = 0
end

local cfg = {
	-- DEBUGGING --
	-- Path to premade script. Script generator options will be obviously ignored
	--script_path = 'test_scripts/broken_overlay.script',
	-- Saves generated script to disc
	dump_script = true,
	-- Don't cleanup temporary directory after generation
	keep_tmp = false,
	-- Cache ffprobe results onto file "probed_media". Will do that after parallel probing and after sucessful generation
	-- Also saves probed temporary files. It's a good idea to remove that file after a while since it might indefenitely grow and collisions might take place
	cache_probe = false,
	-- Execute that command after sucessful generation
	-- The string is passed to string.format and will be provided with only 1 argument: path to the result
	--end_exec = '(exec mpv --pause "%s" &)',
	-- When true uses ffmpeg -f concat demuxer. Will be playable in some players only (mpv works). Sometimes resulting clips can be broken
	-- Turn it on if you need to merge a lot of clips and you are low on memory
	fast_concat = false,
	-- DEBUGGING END --

	-- Amount of parallel workers to use for clip generation
	parallel = 4,
	-- Amount of parallel workers to use for initial media probing
	parallel_probe = 16,
	-- Can be:
	-- Path to a text file containing list of videos
	-- Path to a directory with media sources (directory will be read recursively)
	-- Table containing list of files
	-- In all cases that field will be converted to a table with list of sources
	sources = 'list.txt',
	-- Generated output name. Can be function to generate for example random or sequenced names
	out_name = 'out/out_'..tostring(math.random(0, math.maxrandint)),
	-- Name of script generator. You shouldn't change that currently
	script_generator = 'script',
	-- Scripter configuration. You can either specify it here as a table or load from file
	script = tool.load_lua_table('configs/default.script_cfg'),
	-- TO DO: Allow overrding ALL scripter options from here
	-- NOTE: Following 2 options will override values with same names found in script
	-- Final clip duration in seconds
	max_duration = 90,
	-- Maximum amount of clips allowed to be produced
	max_clips = 9999
}
if not cfg.script_path and not cfg.script then
	cfg.script = random_script_config()
	--cfg.script = load_script_config('configs/lazy_scary.script_cfg')
end
--fixate_clip(cfg.script, 'EARRAEP COLLAB 2-6TuwzxQmhPU.mp4')

return cfg
