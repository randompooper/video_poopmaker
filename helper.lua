require("core")
local tool = require("tool")
local effect = require("effect")

-- Helper functions used by config for script generation
-- You must set helper.scripter to scripter for some functions to work
local helper = {}

function helper.random_bool()
	return math.random(0, 1) == 1
end

function helper.random_float(a, b)
	return math.random() * (b - a) + a
end

helper.random_int = math.random

function helper.random_scale()
	local scale = math.random() * 1.8 + 1.2
	local v = tool.video_size
	local x = math.random(0, math.floor(v.w * scale - v.w))
	local y = math.random(0, math.floor(v.h * scale - v.h))
	return { scale = scale, x = x, y = y }
end

function helper.overlay_clip(cfg, pick, interval, clips)
	local scr = helper.scripter.generate(cfg, clips, pick)
	if #scr == 0 then
		return scr
	end
	scr = scr[1]
	if interval ~= nil then
		-- Can happen when clip_override effect is found and parameters instructed scripter to use whole video
		if scr.interval == nil then
			scr.interval = { 0 }
		end
		-- Try to keep the same duration as the picked clip
		scr.interval = { scr.interval[1], scr.interval[1] + (interval[2] - interval[1]) }
	end
	local v = tool.video_size
	return {
		scr = scr,
		x = math.random(0, v.w - 200), y = math.random(0, v.h - 150),
		size = math.random(120, 240),
		force_stretch = true
	}
end

function helper.random_overlay_clip(clip, clips, interval, cfg)
	return helper.overlay_clip(cfg, clips[math.random(1, #clips)], interval, clips)
end

local sound_cache = nil
function helper.random_sound(path)
	if sound_cache == nil then
		sound_cache = tool.list_files(path)
	end
	local pick = sound_cache[math.random(1, #sound_cache)]
	return { pick }
end

function helper.random_clip_sound(clip, clips, interval)
	local pick = clips[math.random(1, #clips)]
	local duration = interval and (interval[2] - interval[1]) or tool.media_duration(clip)
	return { pick, tool.random_interval(tool.media_duration(pick), duration, duration) }
end

local music_cache = nil
function helper.random_dance(music, min_v, max_v, clip, clips, interval)
	if music_cache == nil then
		music_cache = tool.list_files(music)
	end
	local pick = music_cache[math.random(1, #music_cache)]
	local length = tool.media_duration(pick)
	local interval = { 0, length > min_v and helper.random_float(min_v, max_v) or length }
	return { music = pick, interval = interval, count = math.random(8, 10) }
end

-- Returns value itself or extracts it if value is a function or a string pointing to a function from this object
function helper.extract_value(v, ...)
	-- If function then evaluate
	if type(v) == 'function' then
		v = v(...)
	end
	-- Look if there is a helper function to evaluate
	if type(v) == 'string' and string.find(v, '^helper.') then
		v = load(string.format('return function(...) return %s end', v), 'script.lua::helper', 't', { math = math, helper = helper })()(...)
	end
	return v
end

-- Calls effect hooks and approximates clip duration. Effect hooks might change clip interval and effect options
function helper.hook_clip_duration(clip, interval, eff)
	local duration = interval and interval[2] - interval[1] or tool.media_duration(clip)
	local eff_hook = eff and effect[eff.name..'_hook']
	return eff_hook and eff_hook(clip, duration, interval, eff.opt) or duration
end

return helper
